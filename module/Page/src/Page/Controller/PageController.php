<?php
namespace Page\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Page\Model\Page;
use Page\Form\PageForm;

class PageController extends AbstractActionController
{
    protected $pageTable; 

    public function indexAction(){
        return new ViewModel(
            array(
                'pages' => $this->getPageTable()->fetchAll(),
            )
        );
    }

    public function deleteAction(){
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('page');
        } 
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No'); 
            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getPageTable()->deletePage($id);
            } 
           return $this->redirect()->toRoute('page');
        } 
        return array(
            'id'    => $id,
            'page' => $this->getPageTable()->getPage($id)
        );
    }

    public function modifyAction(){
        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('page', array(
                'action' => 'add'
            ));
        }

        $page = $this->getPageTable()->getPage($id);
 
        $form  = new PageForm();
        $form->bind($page);
        $form->get('submit')->setAttribute('value', 'Редактировать');
 
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($page->getInputFilter());
            $form->setData($request->getPost());
 
            if ($form->isValid()) {
                $this->getPageTable()->savePage($form->getData());
                return $this->redirect()->toRoute('page');
            }
        }
 
        return new ViewModel(array(
            'id' => $id,
            'form' => $form,
        ));
    }

    public function addAction(){
        $form = new PageForm();
        $form->get('submit')->setValue('Добавить');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $page = new Page();
            $form->setInputFilter($page->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $page->exchangeArray($form->getData());
                $this->getPageTable()->savePage($page);

                return $this->redirect()->toRoute('page');
            }
        }
        
        return new ViewModel(array('form' => $form));
    }

    public function getPageTable(){
        if(!$this->pageTable){
            $sm = $this->getServiceLocator();
            $this->pageTable = $sm->get('Page\Model\PageTable');
        }
        return $this->pageTable;
    }
}
