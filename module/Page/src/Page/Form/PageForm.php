<?php
namespace Page\Form;
 use Zend\Form\Form;
 class PageForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'article',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
                'label' => 'Содержание',
            ),
        ));

        $this->add(array(
            'name' => 'Public',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Публикация',
            ),
        ));

         $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Название',
            ),
        ));

         $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Добавить',
                'id' => 'submitbutton',
            ),
        ));
    }
}